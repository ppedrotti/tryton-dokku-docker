#!/bin/bash

set -e

: ${DB_USER:=${POSTGRES_ENV_POSTGRES_USER:='postgres'}}
: ${DB_PASSWORD:=${POSTGRES_ENV_POSTGRES_PASSWORD}}
: ${DB_HOSTNAME:=${POSTGRES_PORT_5432_TCP_ADDR:='postgres'}}
: ${DB_PORT:=${POSTGRES_PORT_5432_TCP_PORT:='5432'}}
#: ${TRYTOND_DATABASE_URI:="postgresql://${DB_USER}:${DB_PASSWORD}@${DB_HOSTNAME}:${DB_PORT}/"}
: ${PYTHONOPTIMIZE:=1}

#set ${TRYTOND_DATABASE_URI=postgresql://postgres:0b68aeaad1bb71d999737ef8d4a25633@dokku-postgres-gnuhealthd:5432}

TRYTOND_DATABASE_URI="postgresql://postgres:0b68aeaad1bb71d999737ef8d4a25633@dokku-postgres-gnuhealthd:5432"

export TRYTOND_DATABASE_URI PYTHONOPTIMIZE

trytond-admin -d gnuhealthd --all --update-modules-list

exec "$@"
